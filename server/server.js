var server = require('http').createServer();
var io = require('socket.io')(server);
    io.set( 'origins', '*:*' );
var refresco_lista_espera = 60*3; //3 minutos
var max_intentos_lista_espera = 3; //limite de intentos para poder buscar taxista en listas de espera, si este valor llega a 3 se anulará el envió. y se deberá solicitar el taxista de manera manual.
var tiempo_para_rankear = 60*10; //15 minutos luego de que se haga la entrega para recordar a la cliente que rankee al taxista.
console.log('Servidor Iniciado');
//Conectar BD
var pizzasdb = require('pizzasdb');
var chat = require('chat');
var Group = require('GrupoClass').Group;

var clientes = new Group();
var taxistas = new Group();
var envios = new Group();
var solicitudes = new Group();
var lista_espera = new Group();

io.sockets.on('connection', function (cliente) {
    cliente.emit('onopen','');
    
    cliente.on('connectID',function(id){
        //Asignar ID
        cliente.id = id.id;
        cliente.type = id.type;
        if(cliente.type=='cliente'){
            if(clientes.getFromId(id.id)==null){
                clientes.add(cliente);
            }
            else{
                clientes.update(cliente);
            }
             pizzasdb.conectarSucursal(cliente.id);
        }
        else{
            cliente.enlaces = new Group();
            cliente.lat = 0;
            cliente.lon = 0;
            if(taxistas.getFromId(id.id)==null){
                taxistas.add(cliente);
            }
            else{
                taxistas.update(cliente);
            }        
        }
    });
    
    //Funcion para refrescar las coordenadas del taxista.
    cliente.on('refreshLocation',function(data){
        /*Seguir usando la api pero aqui refrescar el mapa de los que tienen conexion con este taxista.*/
    });
    
    //Funciones Particulares, borrar cuando se reutilice el codigo en otro proyecto
    cliente.on('buscarRepartidor',function(idPedido){
        buscarRepartidor(cliente,idPedido);
    });
    
    //Programar envios
    cliente.on('programar',function(idPedido){
        reintentarLuego(idPedido,cliente,idPedido.time);        
        console.log('Envio programado');
    });
    
    //Aqui es cuando el taxista acepta o rechaza la solicitud
    cliente.on('responderSolicitud',function(data){
        console.log(data);
        if(data.response){
            console.log('Solicitud aceptada');
            envio = envios.getFromId(data.idPedido);
            //Guardar datos del taxista
            pizzasdb.addRepartidor(data.idPedido,envio.candidatos[0].tiempoDeRespuesta,cliente.id,envio.candidatos[0].tiempoDeLlegada);
            envio.cliente.emit('respuestaSolicitud',{response:true,data:data,tiempoDeRespuesta:envio.candidatos[0].tiempoDeRespuesta,taxista:cliente.id});
        }else{
            console.log('Solicitud rechazada');
            //Sacar el taxista de los candidatos y asignar a otro
            var c = [];
            for(var i in envio.candidatos){
                if(parseInt(envio.candidatos[i].taxista_id)!==parseInt(cliente.id)){
                    c.push(envio.candidatos[i]);
                }
            }
            envio.candidatos = c;
            if(envio.candidatos.length===0){//Se acabaron los candidatos                
                envio.cliente.emit('respuestaSolicitud',{response:false});
                reintentarLuego(data,envio.cliente);                
            }
            else{ //Se le envia la solicitud al siguiente
                c = taxistas.getFromId(envio.candidatos[0].taxista_id);
                c.emit('onElegido',{idPedido:data.idPedido});
            }
        }
        solicitudes.remove(cliente);
    });
    
    //Enviar un mensaje
    cliente.on('sendMessage',function(data){
        if(data.pedido_id!==undefined && data.dest!==undefined){
            var grupo = cliente.type==='cliente'?taxistas:clientes;
            var destinatario = grupo.getFromId(data.dest);
            chat.addMessage(data);
            if(destinatario!==null){
                destinatario.emit('message_receiver',data);
            }
        }
    });
    
    //Unir
    cliente.on('enlazar',function(repartidor){        
         var taxi = taxistas.getFromId(repartidor.taxistas_id);
         if(taxi.enlaces.getFromId(cliente.id)===null){
             taxi.enlaces.add(cliente);
             cliente.emit('refreshTaxista',{lat:taxi.lat,lon:taxi.lon});
         }
    });
    
    //Actualizar ubicacion
    cliente.on('latLng',function(data){
       cliente.lat = data.lat;
       cliente.lon = data.lon;
       if(data.clientes_id!==undefined && (clientes.getFromId(data.clientes_id)!==null && cliente.enlaces.getFromId(data.clientes_id) === null)){
           cliente.enlaces.add(clientes.getFromId(data.clientes_id));
       }
       for(var i in cliente.enlaces.list){
           cliente.enlaces.list[i].emit('refreshTaxista',{lat:cliente.lat,lon:cliente.lon});
       }
    });
    
    cliente.on('disconnect',function(reasonCode, description){        
        switch(cliente.type){
            case 'cliente': 
                pizzasdb.desconectarSucursal(cliente.id);
                clientes.remove(cliente);
                //deslinkear 
                desenlazar(cliente);
            break;
            default: 
                pizzasdb.desconectarRepartidor(cliente.id);
                taxistas.remove(cliente);                 
            break;
        }
    });
    
    cliente.on('travelcancel',function(data){
        console.log('Viaje cancelado');
        if(typeof(data.taxistas_id)!=='undefined'){
            var t = taxistas.getFromId(data.taxistas_id);
            if(t!==null){
                t.emit('travelcancel');
            }
        }
    });
});

function desenlazar(cliente){
    for(var i in taxistas.list){
        taxistas.list[i].enlaces.remove(cliente);
    }
}

function buscarRepartidor(cliente,idPedido){
    //console.log('Buscando taxista');
    pizzasdb.seleccionarCandidato(idPedido,function(encontrado){
        //console.log('Respuesta de la bd')        
        if(encontrado){ //Filtrar los que esten desconectados
            newEnc = [];
            for(var i in encontrado){
                if(encontrado[i].taxista_id!==undefined && taxistas.getFromId(encontrado[i].taxista_id)!==null){
                    newEnc.push(encontrado[i]);
                }
            }            
            //Se vuelve a signar lista ya limpiada
            encontrado = newEnc;
            if(encontrado.length>0){
                c = taxistas.getFromId(encontrado[0].taxista_id);
                if(solicitudes.getFromId(c)===null){
                    c.emit('onElegido',{idPedido:idPedido.idPedido});                    
                    solicitudes.add(c);
                }
                envio = {};
                envio.candidatos = encontrado;
                envio.cliente = cliente;
                envio.id = idPedido.idPedido;
                e = envios.getFromId(envio.id);
                if(e===null){
                    envios.add(envio);
                    //Enviar la primera solicitud                
                }
                else{
                    envios.update(envio);
                }
            }
            else{
                cliente.emit('respuestaSolicitud',{response:false});
                reintentarLuego(idPedido,cliente);
            }
        }else{
            cliente.emit('respuestaSolicitud',{response:false});            
            reintentarLuego(idPedido,cliente);
        }
    //console.log(encontrado);
    //Buscar a los taxistas y ver si alguno quiere el trabajo. 
});
}

function reintentarLuego(idPedido,cliente,time){
    time = time===undefined?refresco_lista_espera:time;
    if(lista_espera.getFromId(idPedido.idPedido)===null){
        lista_espera.add({id:idPedido.idPedido,idPedido:idPedido, time:time, cliente:cliente,intentos:1,type:'buscarRepartidor'});
    }
    else{
        if(lista_espera.list[lista_espera.getPosition(idPedido.idPedido)].intentos<max_intentos_lista_espera){
            lista_espera.list[lista_espera.getPosition(idPedido.idPedido)].time = time;
            lista_espera.list[lista_espera.getPosition(idPedido.idPedido)].intentos++;
        }   
        else{
            lista_espera.remove({id:lista_espera.list[lista_espera.getPosition(idPedido.idPedido)].id});
        }
    }
}

function reloj(){    
    if(lista_espera.list.length>0){       
        for(i in lista_espera.list){            
            if(lista_espera.list[i].time<=0){
                switch(lista_espera.list[i].type){
                    case 'buscarRepartidor': 
                        buscarRepartidor(lista_espera.list[i].cliente,lista_espera.list[i].idPedido);
                    break;
                    case 'rankear': 
                        lista_espera.list[i].cliente.emit('rankearRepartidor',{idPedido:lista_espera.list[i].id,taxistas_id:lista_espera.list[i].taxistas_id});
                        lista_espera.remove(lista_espera.list[i]);
                    break;
                }            
            }else{
                lista_espera.list[i].time--;
            }
        }
    }
    else{
        
    }
    setTimeout(reloj,1000);
}
reloj();
server.listen(3000);