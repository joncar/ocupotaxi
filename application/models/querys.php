<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    //Variables de 6x1 para el calculo de el 6x1
    var $referidos = array();
    
    function __construct()
    {
        parent::__construct();
    } 
    
    function getUsuarios(){        
        return $this->db->get('clientes')->num_rows;
    }
    
    function getTaxistasOnline(){
        $taxistas = array();
        $this->db->where('status != ',5);
        foreach($this->db->get('taxistas')->result() as $m){
            array_push($taxistas,array('id'=>$m->id,'lat'=>$m->lat,'lon'=>$m->lon,'nombre'=>$m->nombre_taxista));
        }
        return $taxistas;
    }
    
    function getTaxisOcupados(){
        $taxistas = array();
        $this->db->where('status != ',3);
        $this->db->where('pedidos.taxistas_id IS NOT NULL',NULL,TRUE);
        foreach($this->db->get('pedidos')->result() as $m){
            array_push($taxistas,array('lat'=>$m->lat,'lon'=>$m->lon,'nombre'=>$m->id));
        }
        return $taxistas;
    }
    
    function getTaxisLibres(){        
        return $this->db->get_where('taxistas',array('status !='=>5))->num_rows();
    }
       
    
    function getEntregas(){
        $this->db->order_by('id','DESC');
        $this->db->limit('10');
        $this->db->where('status',3);
        $transacciones = array();
        foreach($this->db->get('pedidos')->result() as $t){
            array_push($transacciones, $t);
        }
        return $transacciones;
    }        
    
    function getServiciosSucursalDestino(){
        $this->db->order_by('id','DESC');
        $this->db->limit('10');
        $this->db->where('pedidos.status',2);
        $this->db->select('pedidos.*, clientes.nombre_cliente as cliente');
        $this->db->join('clientes','clientes.id = pedidos.clientes_id');        
        $transacciones = array();
        foreach($this->db->get('pedidos')->result() as $t){
            array_push($transacciones, $t);
        }
        return $transacciones;
    }
    
    function get_resumenes(){
        $resumenes = array();        
        $resumenes['taxisOcupados'] = $this->getTaxisOcupados();
        $resumenes['taxisLibres'] = $this->getTaxisLibres()-count($resumenes['taxisOcupados'] );
        $resumenes['taxisLibres'] = $resumenes['taxisLibres']<0?0:$resumenes['taxisLibres'];
        $resumenes['usuarios'] = $this->getUsuarios();        
        $resumenes['taxistasOnline'] = $this->getTaxistasOnline();        
        $resumenes['entregas'] = $this->getEntregas();        
        $resumenes['servicios'] = $this->getServiciosSucursalDestino();
        return $resumenes;
    }
}
?>
