<div id="page-wrapper" style="min-height: 722px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-dashboard"></i> Administración</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-taxi fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge"><?= count($resumenes['taxistasOnline']) ?></h2>
                                <div>#Taxi Online</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-taxi fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge"><?= count($resumenes['taxisOcupados']) ?></h2>
                                <div>#Taxis Ocupados!</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-taxi fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge"><?= $resumenes['taxisLibres'] ?></h2>
                                <div>Taxis Libres!</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="huge"><?= $resumenes['usuarios'] ?></h2>
                                <div>Usuarios Activos!</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div>

                    <!-- Mapas -->
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#home" id="mapaenvivoa" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i> Mapa en vivo</a></li>
                      <li role="presentation"><a href="#profile" id="mapatraficoa" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-bus"></i> Ver Tráfico</a></li>                      
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div id="mapaenvivo" style="height:500px;">
                                Mapa
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div id="mapatrafico" style="height:500px;">
                                Mapa
                            </div>
                        </div>
                    </div>

                </div>                               
                <!-- /.panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-clock-o fa-fw"></i> Ultimos 10 servicios
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <ul class="timeline">
                            <?php foreach($resumenes['servicios'] as $s): ?>
                            <li>
                                <div class="timeline-badge"><i class="fa fa-check"></i>
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4 class="timeline-title"><?php $s->cliente ?></h4>
                                        <p><small class="text-muted"><i class="fa fa-clock-o"></i> <?php $s->fecha_solicitud ?></small>
                                        </p>
                                    </div>
                                    <div class="timeline-body">
                                        <p><?= 'Servicio realizado para '.$s->cliente.' con el nro de viaje: '.$s->id ?></p>
                                    </div>
                                </div>
                            </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!-- /.col-lg-8 -->            
        </div>
        <!-- /.row -->
    </div>
<script src="http://maps.googleapis.com/maps/api/js?signed_in=true&v=3.exp&sensor=true"></script>
<script>
    var marks = [];
    var map;
    function initMap(){
        <?php $coords_default = $this->db->get('ajustes')->row(); ?>        
            var mapaContent = document.getElementById('mapaenvivo');
            <?php if(!empty($resumenes['taxistasOnline'])): ?>
                var center = new google.maps.LatLng(<?= empty($resumenes['taxistasOnline'][0])?0:$resumenes['taxistasOnline'][0]['lat'] ?>,<?= empty($resumenes['taxistasOnline'][0])?0:$resumenes['taxistasOnline'][0]['lon'] ?>);
            <?php else: ?>
                var center = new google.maps.LatLng<?= $coords_default->mapa ?>;
            <?php endif ?>
            var mapOptions = {
                        zoom: <?= $coords_default->zoom ?>,
                        center: center,               
            };
            map = new google.maps.Map(mapaContent, mapOptions);                            
            var center = new google.maps.LatLng<?= $coords_default->mapa ?>;
            var maptrafic = new google.maps.Map(document.getElementById('mapatrafico'), {
                zoom: <?= $coords_default->zoom ?>,
                center: center
              });

              var trafficLayer = new google.maps.TrafficLayer();
              trafficLayer.setMap(maptrafic);              
    }
    
    function refresh(){
        $.post('<?= base_url('panel/dashboard') ?>',{},function(data){
            data = JSON.parse(data);
            console.log(marks);
            if(marks.length>0){
                //Verificamos los que no estan en la nueva lista y se sacan
                for(i in marks){
                    var encontrado = false;
                    for(k in data.taxistasOnline){
                        if(data.taxistasOnline[k].id==marks[i].id){
                            encontrado = true;                       
                        }
                    }
                    if(!encontrado){
                        marks[i].marker.setMap(null);
                        //Refrescamos la lista
                        var aux = [];
                        for(p in marks){
                            if(p!=i){
                                aux.push(marks[i]);
                            }
                        }
                        marks = aux;
                    }
                }
            }
            
            for(var i in data.taxistasOnline){
                    //Verificamos si existe en la lista de taxistas si no lo añadimos
                    var d = data.taxistasOnline[i];                    
                    var existe = false;
                    for(var k in marks){
                        if(d.id==marks[k].id){
                            marks[k].marker.setPosition(new google.maps.LatLng(d.lat,d.lon));
                            existe = true;
                        }
                    }
                    if(!existe){
                        var t = new marksObject(d);
                        marks.push(t);
                    }
            }            
            setTimeout(refresh,2000);
        });                
    }
    
    function marksObject(d){
        this.id = d.id;
        this.marker = new google.maps.Marker({ position: new google.maps.LatLng(d.lat,d.lon), map: map, title: d.nombre,icon:'<?= base_url('img/taxiLibre.png') ?>' });
        this.marker.infowindow = new google.maps.InfoWindow({
            content:'<div><ul><li>Nombre: '+d.nombre+'</li></ul></div>'
         });
         this.marker.infowindow.opened = false;
         
        this.marker.isInfoWindowOpen = function(){            
            return this.infowindow.opened;
        };
        google.maps.event.addListener(this.marker, "click", function (e) { 
            this.infowindow.open(map,this);
            this.infowindow.opened = true;
         });
        google.maps.event.addListener(this.marker, "dblclick", function (e) { 
            map.panTo(new google.maps.LatLng(e.latLng.lat(),e.latLng.lng()));
            map.setZoom(18);
         });
         
         google.maps.event.addListener(this.marker.infowindow, "closeclick", function (e) {
            this.opened = false;
        });
    }
    
    refresh();
    
    initMap();
    $("#mapatraficoa, #mapaenvivoa").on('shown.bs.tab',function(){
        initMap();
    });
</script>