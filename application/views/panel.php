<div class="page-header">
        <h1>
                Escritorio
                <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>                        
                </small>
        </h1>
</div><!-- /.page-header -->

<div class="row">
        <div class="col-xs-12" id="dashboardPanel">
               <?php if(empty($crud)): ?>
               <?php $this->load->view('includes/dashboard') ?>
               <?php else: ?>
               <?php $this->load->view('cruds/'.$crud) ?>
               <?php endif ?>
        </div><!-- /.col -->
</div><!-- /.row -->