<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Equipo extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function equipos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);            
            $output = $crud->render();
            $this->loadView($output);
        }
        function socios($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);            
            $output = $crud->render();
            $this->loadView($output);
        }
        function seguros($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);            
            $output = $crud->render();
            $this->loadView($output);
        }
    }
?>
