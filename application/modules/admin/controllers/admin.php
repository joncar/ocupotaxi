<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function pedidos(){
            $crud = $this->crud_function('','');
            $crud->display_as('repartidores_id','Repartidor')
                 ->display_as('direccion','Direccion de recogida')
                 ->display_as('direccion_entrega','Direccion de entrega');
            $crud->set_relation('taxistas_id','taxistas','nombre_taxista');
            $crud->columns('fecha_solicitud','direccion','direccion_entrega','trayecto','tiempo_transito','tiempo_recoleccion','tiempo_entrega','tiempo_total','taxistas_id','status');
            $crud->field_type('status','dropdown',array('-1'=>'<span class="label label-danger">Cancelado</span>','1'=>'<span class="label label-default">Por recoger</span>','2'=>'<span class="label label-info">En camino</span>','3'=>'<span class="label label-success">Entregado</span>','4'=>'<span class="label label-warning">Programado</span>'))
                 ->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_export()
                 ->unset_print();
            $output = $crud->render();
            $this->loadView($output);
        }
    }
?>
