<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Taxista extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function taxistas($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);
            $crud->field_type('password','password')
                    ->field_type('status','dropdown',array('1'=>'Esperando trabajo','2'=>'Realizando Envio','3'=>'Sin espacio','4'=>'Siniestrado','5'=>'Inactivo','6'=>'Bloqueado'))                    
                    ->field_type('ubicacion','hidden')
                    ->field_type('tarifa','hidden',0)
                    ->field_type('status','dropdown',array('1'=>'Activo','5'=>'Inactivo'))
                    ->field_type('calificacion','hidden',0)
                    ->field_type('capacidad_carga','hidden',$this->db->get('ajustes')->row()->max_tareas_simultaneas)
                    ->field_type('ubicacion','hidden')
                    ->field_type('tipo_pago','dropdown',array('1'=>'Por contrato','2'=>'Por distancia'))
                    ->field_type('fecha_registro','hidden',date("Y-m-d H:i:s"))
                    ->field_type('lat','invisible')
                    ->field_type('lon','invisible');
            
            if($crud->getParameters()=='list' || $crud->getParameters()=='ajax_list' || $crud->getParameters()=='success'){
                $crud->set_field_upload('foto','images/taxistas');
            }else{
                $crud->field_type('foto','image',array('path'=>'images/taxistas','width'=>'300px','height'=>'300px'));
            }            
            $crud->unset_edit_fields('email');
            $crud->set_rules('email','Email','required|valid_email|is_unique[taxistas.email]')
                    ->set_rules('password','Password','required|min_length[8]');
            
            $crud->callback_before_insert(array($this,'addCoords'));
            $crud->callback_before_update(array($this,'addCoords'));
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function addCoords($post){
            $p = str_replace('(','',$post['ubicacion']);
            $p = str_replace(')','',$p);
            $p = explode(',',$p);
            $post['lat'] = trim($p[0]);
            $post['lon'] = trim($p[1]);
            return $post;
        }
        
        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->field_type('mapa','map');
            $output = $crud->render();
            $this->loadView($output);
        }
    }
?>
