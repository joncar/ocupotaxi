<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Cliente extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function clientes($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);            
            $crud->field_type('ubicacion','map');
            $crud->field_type('password','password')
                 ->field_type('lat','invisible')
                 ->field_type('lon','invisible');
            $crud->set_rules('email','Email','required|valid_email|is_unique[sucursales.email]')
                 ->set_rules('password','Password','required|min_length[8]');
            
            $crud->callback_before_insert(array($this,'addCoords'));
            $crud->callback_before_update(array($this,'addCoords'));
            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function transacciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y,$this);                        
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function pedidos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('ubicacion','map')
                    ->field_type('tipo_pago','dropdown',array('1'=>'Efectivo','2'=>'Debito','3'=>'Credito'))
                    ->field_type('status','dropdown',array('-1'=>'Cancelado','1'=>'Por recoger','2'=>'En camino','3'=>'Entregado','4'=>'Programado'));
            
            $output = $crud->render();
            $this->loadView($output);
        }
        
         function addCoords($post){
            $p = str_replace('(','',$post['ubicacion']);
            $p = str_replace(')','',$p);
            $p = explode(',',$p);
            $post['lat'] = trim($p[0]);
            $post['lon'] = trim($p[1]);
            return $post;
        }
        
        function estado_cuenta(){
            $this->as = array('estado_cuenta'=>'transacciones');
            $crud = $this->crud_function('','');
            $crud->set_subject('Estados de cuenta de sucursales');
            $crud->set_relation('sucursales_id','sucursales','{nombre_sucursal}');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_columns('tipo')
                 ->display_as('sucursales_id','Sucursal');
            $crud->where('tipo',1);
            $output = $crud->render();
            $output->title = 'Estado de cuenta de sucursal';
            $this->loadView($output);
        }
    }
?>
