<?php
require_once APPPATH.'/controllers/main.php';
require_once APPPATH.'/libraries/Conekta.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Api extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                $this->load->model('querys');
        }
        
        public function loadView($data)
        {
             if(!empty($data->output)){
                $data->view = empty($data->view)?'panel':$data->view;
                $data->crud = empty($data->crud)?'user':$data->crud;
                $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        public function ajustes($x = '',$y = ''){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete()
                     ->unset_edit()
                     ->unset_add();            
            $crud = $crud->render();
            $this->loadView($crud);
        }    
        
        public function taxistas($x = '',$y = ''){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete();
            $crud->unset_fields('fecha_registro');
            if(empty($y) || !empty($_POST) && $_POST['email']!=$this->db->get_where('taxistas',array('id'=>$y))->row()->email){
                $crud->set_rules('email','Email','required|valid_email|is_unique[usuarios.email]');
            }
            $crud->required_fields('email','password','nombre_taxista');
            $crud = $crud->render();
            $this->loadView($crud);
        }    
        
         public function clientes($x = '',$y = ''){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete();
            $crud->unset_fields('fecha_registro');
            $crud->edit_fields('responsable','nombre_cliente','contrato','telefono_fijo','telefono_celular','password');
            $crud->set_rules('email','Email','required|valid_email|is_unique[clientes.email]');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function refreshLocation(){
            $data = array();
            if(!empty($_POST['lat']) && !empty($_POST['lon'])){
                $data['lat'] = $_POST['lat'];
                $data['lon'] = $_POST['lon'];
                $data['ubicacion'] = '('.$_POST['lat'].','.$_POST['lon'].')';
            }
            if(!empty($_POST['status'])){
                $data['status'] = $_POST['status'];
                if($data['status']==1){
                    $pedidos = $this->db->get_where('pedidos',array('taxistas_id'=>$_POST['id'],'status !='=>3));
                    if($pedidos->num_rows>0){
                        $data['status'] = 6;
                    }
                }
            }
            
            $this->db->update('taxistas',$data,array('id'=>$_POST['id']));
            echo json_encode(array());
        }
        
        public function pedidos(){
            $crud = $this->crud_function('',''); 
            $crud->set_relation('taxistas_id','taxistas','nombre_taxista');
            $crud->callback_column('direccion',function($val){
               return $val; 
            });
            $crud->callback_before_insert(function($post){
                $post['fecha_solicitud'] = date("Y-m-d H:i:s",strtotime($post['fecha_solicitud']));
                $post['status'] = 1;
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('mensajes',array('pedidos_id'=>$primary,'mensajes'=>'[]'));                
                return true;
            });
            if($crud->getParameters()=='update' || $crud->getParameters()=='update_validation'){
                $crud->required_fields('status');
            }
            $crud = $crud->render();
            $this->loadView($crud);
            
        }
        
        public function pedidosMotos(){
            $this->as = array('pedidosMotos'=>'pedidos');
            $crud = $this->crud_function('',''); 
            $crud->where('pedidos.status <',3);
            $crud->callback_column('direccion',function($val){
               return $val; 
            });
            $crud->callback_before_insert(function($post){
                $post['fecha_solicitud'] = date("Y-m-d H:i:s");
                return $post;
            });
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
            
        }
        
        public function transacciones(){
            $crud = $this->crud_function('','');
            $crud->callback_before_insert(function($post){
                $post['fecha'] = date("Y-m-d H:i:s");
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->sendRecibo($_POST['pedido_id'],$primary);
            });
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
            
        }
    
    public function getMensajes(){   
        $this->form_validation->set_rules('pedido_id','ID','required');            
        if($this->form_validation->run()){
            $mensajes = $this->db->get_where('mensajes',array('pedidos_id'=>$this->input->post('pedido_id')));
            if($mensajes->num_rows>0){
                echo json_encode($mensajes->row()->mensajes);
            }
            else{
                echo json_encode(array());
            }
        }
        else{
            echo json_encode(array());
        }
    }        
    
    public function getStructure($table){
        return $this->db->field_data($table);
    }
    
    public function sendRecibo($pedidoId,$nrorecibo){
        $this->db->select('pedidos.*, clientes.nombre_cliente as cliente, taxistas.nombre_repartidor as repartidor');
        $this->db->join('clientes','clientes.id = pedidos.clientes_id');
        $this->db->join('taxistas','taxistas.id = pedidos.taxistas_id');
        $pedido = $this->db->get_where('pedidos',array('pedidos.id'=>$pedidoId));
        if($pedido->num_rows>0){
            $pedido = $pedido->row();
            $subtotal = (($pedido->precio*0.16)-$pedido->precio)*-1;
            $iva = ($pedido->precio*0.16);
            $msj = "
                <table border='1' width='100%'>
                    <thead style='background:#D9D9D9'>
                        <tr><th align='left'>RECIBO</th><th align='left' colspan='2'>No.</th></tr>
                    </thead>
                    <tbody>
                        <tr><td align='left'><b>DESCRIPCIÓN: </b> ".$pedido->cliente."</td><td align='left' colspan='2'>".$nrorecibo."</td></tr>
                        <tr>
                            <td align='center' rowspan='3'><img src='".base_url('img/logo.png')."' style='width:70%'></td>
                            <th align='right'>SubTotal</th>
                            <td align='right'>$".$subtotal."</td>
                        </tr>
                        <tr>
                            <th align='right'>IVA</th>
                            <td align='right'>$".$iva."</td>
                        </tr>
                        <tr>
                            <th align='right'>Total</th>
                            <td align='right'>$".$pedido->precio."</td>
                        </tr>
                        <tr>
                            <td align='left'>Fecha: ".date("d-m-Y H:i:s",strtotime($pedido->fecha_solicitud))."</td>
                            <td align='left' colspan='3'>Recibido por: ".$pedido->repartidor."</td>
                        </tr>
                    </tbody>
                </table>
                <p>Si el cargo fue realizado con tarjeta de Débito o Crédito, en su estado de cuenta bancario aparecerá una leyenda con el nombre de Conekta relacionado a este recibo.</p>
                <p align='center'>Para nosotros fue un placer atenderle</p>
                <p>Para	cualquier duda, por favor envíe un correo a <a href='mailto:info@deliverymix.com.mx'>info@deliverymix.com.mx</a> o ingrese a <a href='http://www.deliverymix.com.mx'>www.deliverymix.com.mx</a> donde con gusto le atenderemos.</p>
            ";

            correo('joncar.c@gmail.com, sulkin@gmail.com','Hola '.$pedido->cliente.' te enviamos tu recibo de pago',$msj);
        }
    }
    
    public function calificaciones(){        
        $crud = $this->crud_function('','');                 
        $crud->callback_after_insert(function($post){
            $calificaciones = get_instance()->db->get_where('calificaciones',array('taxistas_id'=>$post['taxistas_id']));
            $suma = 0;
            foreach($calificaciones->result() as $s){
                $suma+= $s->calificacion;
            }
            
            get_instance()->db->update('taxistas',array('calificacion'=>$suma/$calificaciones->num_rows),array('id'=>$post['taxistas_id']));
        });        
        $crud = $crud->render();
        $this->loadView($crud);            
    }
    
    public function getBalance(){
        $this->form_validation->set_rules('clientes_id','Sucursal','required|integer|greather_than[0]');
        if($this->form_validation->run()){
            $balance = array();
            //Envios de hoy            
            $balance['envios_hoy'] = $this->db->get_where('pedidos',array('DATE(fecha_solicitud)'=>date("Y-m-d")))->num_rows;
            //Envios del mes
            $balance['envios_mes'] = $this->db->get_where('pedidos',array('MONTH(fecha_solicitud)'=>date("m")))->num_rows;
            //costo de envios
            $balance['envios'] = $balance['envios_mes'] * $this->db->get('ajustes')->row()->precio_por_envio_sin_contrato;
            //costo de envios
            $balance['cobranza'] = $this->db->query('select SUM(pedidos.precio) as total from pedidos where MONTH(fecha_solicitud) = '.date("m"))->row()->total;
            
            echo json_encode($balance);
        }
        echo json_encode(array());
    }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
