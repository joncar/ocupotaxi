// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var MapApp = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'Main'
  })

  // Each tab has its own nav history stack:

  .state('tab.main', {
    url: '/main',
    views: {
      'tab-main': {
        templateUrl: 'templates/main.html'        
      }
    }
  })
  
.state('tab.envio', {
    url: '/envio',
    cache:true,
    views: {
      'tab-main': {
        templateUrl: 'templates/envios.html',
        controller: 'Envios'
      }
    }
  })
  
  .state('confirmar', {
    cache:false,
    url: '/confirmar',
    templateUrl: 'templates/confirmar.html',
    controller: 'Confirmar'    
  })    
  
.state('tab.buscarRepartidor', {
    cache:false,
    url: '/buscarRepartidor',
    views: {
      'tab-main': {
        templateUrl: 'templates/buscarRepartidor.html',
        controller: 'buscarRepartidor'
      }
    }
  })
  
.state('tab.cuenta', {
    cache:false,
    url: '/cuenta',
    views: {
      'tab-main': {
        templateUrl: 'templates/cuenta.html',
        controller: 'Cuenta'
      }
    }
  })
 
  .state('tab.envios', {
    cache:false,
    url: '/envios',
    views: {
      'tab-main': {
        templateUrl: 'templates/envioslist.html',
        controller: 'EnviosList'
      }
    }
  })
  
  .state('tab.envioDetail', {
    cache:false,
    url: '/pedido/:id',
    views: {
      'tab-main': {
        templateUrl: 'templates/enviosdetail.html',
        controller: 'EnviosDetail'
      }
    }
  })
  
  .state('tab.AsignarRepartidor', {
    cache:false,
    url: '/AsignarRepartidor/:id',
    views: {
      'tab-main': {
        templateUrl: 'templates/buscarRepartidor.html',
        controller: 'AsignarRepartidor'
      }
    }
  })
  
  .state('tab.mensajes', {
    cache:false,
    url: '/mensajes',
    views: {
      'tab-main': {
        templateUrl: 'templates/mensajes.html',
        controller: 'Mensajes'
      }
    }
  })
  
  .state('tab.mensajesRead', {
    cache:false,
    url: '/mensajes/:id',
    views: {
      'tab-main': {
        templateUrl: 'templates/mensajesRead.html',
        controller: 'ReadMensajes'
      }
    }
  })
  
  .state('tab.calificar', {
    cache:false,
    url: '/calificar/:id',
    views: {
      'tab-main': {
        templateUrl: 'templates/calificar.html',
        controller: 'Calificar'
      }
    }
  })
  
  .state('tab.ruta', {
    cache:false,
    url: '/ruta',
    views: {
      'tab-main': {
        templateUrl: 'templates/ruta.html',
        controller: 'Ruta'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/main');

});
