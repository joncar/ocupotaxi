MapApp.directive("crudApp", function ($window) {
    return {
        restrict: "AE",
        replace: true,
        template: "<div></div>",
        scope: {
            table:'=',
            crud:'=',
            display_as:'=',
            fieldtypes:'='
        },
        require: 'ngModel',
        link: function (scope, element, attrs,ngModelCtrl) {
                        
            function getStructure(table){
                
            }
            
            function updateModel(offset) {
                ngModelCtrl.$setViewValue(ngModelCtrl.$viewValue);
                ngModelCtrl.$render();
            }

            
            function getDisplay(ele){
                for(i in scope.display_as){
                    if(i==ele){
                        return scope.display_as[i];
                    }
                }
                var str = ele.charAt(0).toUpperCase() + ele.slice(1);
                return str.replace('_',' ');
            };
            
            function getFieldType(ele){
                for(i in scope.fieldtypes){                    
                    if(i==ele){
                        return scope.fieldtypes[i];
                    }
                }
                return 'text';
            };
            
            function edit(){                
                var str = '';
                for(var i in scope.table){
                    var type = getFieldType(i);
                    var label = getDisplay(i);
                    str+= '<label class="item item-input"><input type="'+type+'" placeholder="'+label+'" ng-model="datos.'+i+'"></label>';
                    updateModel(i);
                }
                //str+= '<button type="submit" class="button button-balanced button-full">Guardar</button>';
                element[0].innerHTML = str;
            }
            
            
            switch(scope.crud){
                case 'add': add(); break;
                case 'edit': edit(); break;
                case 'list': list(); break;
            }
        }                                
    }; // end of return
});