angular.module('starter.controllers', [])

.controller('Main', function($scope,$state,$ionicSideMenuDelegate,$ionicPopup,$ionicPlatform,Api,UI,User,SocketConnection,$ionicLoading,$http) {
    $scope.auto = function(el){
        console.log(el);
    }
    $scope = User.getData($scope);
    if(localStorage.lat===undefined){        
        localStorage.lat = '16.7417092';
        localStorage.lon = '-93.0926997';
    }
    if(localStorage.user===undefined){
        document.location.href="index.html";
    }        
   
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };
    
    $scope.newEnvio = function(){
        $scope.loading = UI.getLoadingBox($ionicLoading);
        Api.list('pedidos',{clientes_id:$scope.user,'pedidos.status != ':3},$scope,$http,function(data){ 
            if(data.length>0){
                $state.go('tab.ruta');
            }else{
                $state.go('tab.envio');
            }
        },'where');
    };
    
    $scope.desconectar = function(){
        User.cleanData();
        document.location.href="index.html";
        navigator.app.exitApp();
    };
    
    $scope.connecting = false;
    $scope.connected = function(){$scope.connecting = true; if(!$scope.$$phase){$scope.$apply();}};
    $scope.disconnected = function(){$scope.connecting = false; if(!$scope.$$phase){$scope.$apply();}};
    SocketConnection.WebSocketOpen($scope);
    SocketConnection.connectID($scope.user);
    
    //Background
    // Android customization        
    
    $ionicPlatform.ready(function(){
        Api.list('ajustes',{},$scope,$http,function(data){ 
            Api.ajustes = data[0]; 
        });
        
        Api.list('pedidos',{clientes_id:$scope.user,'pedidos.status != ':3},$scope,$http,function(data){ 
            if(data.length>0){
                localStorage.lat = data[0].lat;
                localStorage.lon = data[0].lon;
                if($state.current.name==='tab.main'){
                    $state.go('tab.ruta');
                }
                else{
                     ws.emit('enlazar',data[0]);
                }
            }
        },'where');
        
        if(typeof(cordova)!=='undefined'){
            cordova.plugins.backgroundMode.setDefaults({ title:'DeliveryMix', text:'Sucursal conectada'});
            // Enable background mode
            cordova.plugins.backgroundMode.enable();
            cordova.plugins.backgroundMode.onactivate = function () {
                cordova.plugins.backgroundMode.configure({
                    title:'App Activo',
                    text:'App Activa'
                });
            };
            if(typeof(Media)!=='undefined'){
                Api.notificationSound = new Media('/android_asset/www/sounds/notificacion.mp3',function(){},function(e){alert("Error getting pos="+e);});
                Api.notificationSound.play();       
            }
        }
    });
        
    
    ws.on('respuestaSolicitud',function(data){
        console.log(data);
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    title:'Respuesta de solicitud.',
                    text:'Haz recibido una respuesta de tu solicitud.'
                });
                Api.notificationSound.play();
        }
        Api.respuestaSolicitudRepartidor(data);
    });
        
    ws.on('message_receiver',function(data){
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    title:'Mensaje recibido.',
                    text:'Te han enviado un mensaje.'
                });
                Api.notificationSound.play();
        }
        
       if($state.current.name==='tab.mensajesRead'){
           $scope.$broadcast('message_receiver',data);
       }
       else{
           var myPopup = $ionicPopup.show({
                template: data.mensaje,
                title: 'Haz recibido un mensaje',
                subTitle: 'Nro. Pedido: '+data.pedido_id,
                scope: $scope,
                buttons: [
                    {
                      text: '<b>Cerrar</b>',              
                      onTap: function(e) {                        
                          myPopup.close();
                      }
                    },
                    {
                      text: '<b>Ver Mensaje</b>',
                      onTap: function(e) {                        
                        document.location.href="#/tab/mensajes/"+data.pedido_id;                        
                      }
                    }
                  ]
          });
       }
    });
    
    $scope.$on('sendmensajetoserver',function(evt,data){
        ws.emit('sendMessage',data);
    });
    
    ws.on('rankearRepartidor',function(data){
        console.log(data);
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    title:'Rankea a tu taxista.',
                    text:'Como te ha ido con tu taxista?.'
                });
                Api.notificationSound.play();
        }
        if($state.current.name!=='tab.calificar'){
            var myPopup = $ionicPopup.show({
                template: data.mensaje,
                title: '¿Como fué la entrega del taxista?',
                subTitle: 'Danós tu opinión sobre la entrega de tu envio #'+data.idPedido,
                scope: $scope,
                buttons: [
                    {
                      text: '<b>Cerrar</b>',              
                      onTap: function(e) {                        
                          myPopup.close();
                      }
                    },
                    {
                      text: '<b>Calificar</b>',
                      onTap: function(e) {                        
                        document.location.href="#/tab/calificar/"+data.taxistas_id;                        
                      }
                    }
                ]
            });
        }
    });
    ws.on('refreshTaxista',function(data){
        $scope.$broadcast('refreshTaxistaCtrl',data);        
    });
    
    $scope.$on('travelcancel',function(evt,data){
        console.log(data);
        ws.emit('travelcancel',data);        
    });
})

.controller('Envios', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);    
    $scope.modal = UI.getModalBox($ionicModal,'templates/modals/programar.html',$scope);
    var f = new Date();
    var fecha_solicitud = f.getFullYear()+'-'+f.getMonth()+'-'+f.getDate()+' '+f.getHours()+':'+f.getMinutes()+':'+f.getSeconds();
    $scope.datos = {ownposition:true,fecha_solicitud:fecha_solicitud,ciudad:'',delegacion:'',ext:''};
    $scope.d = {fechas:'',hora:''};
    $scope.closeProgramar = function(){
        $scope.toggleModal('hide');
    };
    $scope.toggleProgramar = function(datos){
        if(datos.int!==undefined && datos.int!=='' || datos.ownposition){            
            $scope.datos = datos;
            if(!$scope.$$phase){
                $scope.$apply();
            }
            $scope.toggleModal('show');
        }else{
            alert('Debe llenar el campo Int antes de continuar');
        }
    };
    
    $scope.selectFecha = function(datos,d){
        datos.fecha_solicitud = d.fechas.getFullYear()+'-'+d.fechas.getMonth()+'-'+d.fechas.getDate()+' '+d.hora.getHours()+':'+d.hora.getMinutes()+':'+d.hora.getSeconds();
        d.fechas.setHours(d.hora.getHours(), d.hora.getMinutes(), d.hora.getSeconds(), 0);
        var fecha = new Date();
        if(fecha.getTime()>=d.fechas.getTime()){
            alert('Debe asignar una fecha con un intervalo mayor a 2 horas como minimo');
        }else{            
            var m = (d.fechas.getTime()-fecha.getTime())/1000;
            if(m<=7200){
                alert('Debe asignar una fecha con un intervalo mayor a 2 horas como minimo');
            }else{
                $scope.toggleModal('hide');
                $scope.modal = UI.getModalBox($ionicModal,'templates/modals/programar.html',$scope);
                Api.time = m;
                $scope.enviarPedido(datos,true);
            }
        }
    };
    
   $scope.componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        postal_code: 'short_name',
        sublocality_level_1:'long_name'
      };
      
      $scope.inputForm = {
        street_number: 'ext',
        route: 'calle',
        locality: 'delegacion',
        administrative_area_level_1: 'ciudad',
        postal_code: 'cp',
        sublocality_level_1:'colonia'
      };
    
    $scope.selectFieldsFunction = function(place){
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if($scope.componentForm[addressType]){
                var val = place.address_components[i][$scope.componentForm[addressType]];
                $scope.datos[$scope.inputForm[addressType]] = val;
            }
          }
          if(!$scope.$$phase){
              $scope.$apply();
          }
    }
    
    $scope.enviarPedido = function(datos,programar){
        if(datos.int!==undefined && datos.int!=='' || datos.ownposition){
            datos.direccion = datos.direccion===undefined?null:datos.direccion+' '+datos.ciudad;        
            Api.datos = datos;
            Api.programar = programar;
            $state.go('confirmar');
        }else{
            alert('Debe llenar el campo Int antes de continuar');
        }
    };
})

.controller('Confirmar', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope.datos = Api.datos;
    $scope.direccion = Api.datos.direccion;
    $scope.basel = { lat: localStorage.lat, lon: localStorage.lon };
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando Pedido');
    $scope.gps = false;
    $scope.iconMap = undefined;    
    $scope.navigate = false;
    $scope.widthButtonConfirmar = '50%';
    $scope.widthButtonCancelar = '48%';
    
    $scope.unirDireccion = function(){
        Api.datos.direccion = Api.datos.calle===undefined?'':' '+Api.datos.calle;
        Api.datos.direccion+= Api.datos.ext === undefined?'':' ext '+Api.datos.ext;
        Api.datos.direccion+= Api.datos.colonia===undefined?'':' '+Api.datos.colonia;
        Api.datos.direccion+= Api.datos.int===undefined?'':' int '+Api.datos.int;
        Api.datos.direccion+= Api.datos.delegacion===undefined?'':' '+Api.datos.delegacion;
        Api.datos.direccion+= Api.datos.ciudad===undefined?'':' '+Api.datos.ciudad;
        Api.datos.direccion+= Api.datos.cp===undefined?'':' cp '+Api.datos.cp;
        Api.datos.direccion+= Api.datos.localidad===undefined?'':' '+Api.datos.localidad;
    };
    
    $scope.updatePosition = function(){
            if($scope.datos.direccion===null){
            $scope.selectFieldsFunction = function(place){
                
                 if(typeof(place.address_components)!=='undefined'){
                    for (var i = 0; i < place.address_components.length; i++) {
                        console.log(place.address_components);
                        var addressType = place.address_components[i].types[0];
                        if($scope.componentForm[addressType]){
                            var val = place.address_components[i][$scope.componentForm[addressType]];
                            $scope.datos[$scope.inputForm[addressType]] = val;
                            Api.datos[$scope.inputForm[addressType]] = val;
                        }
                      }                      
                      $scope.unirDireccion();            
                      if(!$scope.$$phase){
                          $scope.$apply();
                      }
                      }else{
                            history.back();
                        }                
                }                
                $scope.componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'long_name',
                    postal_code: 'short_name',
                    sublocality_level_1:'long_name'
                };

                $scope.inputForm = {
                    street_number: 'ext',
                    route: 'calle',
                    locality: 'delegacion',
                    administrative_area_level_1: 'ciudad',
                    postal_code: 'cp',
                    sublocality_level_1:'colonia'
                };
            $scope.options = { enableHighAccuracy: true };
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.position=position;
                var c = position.coords;
                $scope.gotoLocation(c.latitude, c.longitude);
                localStorage.lat = c.latitude;
                localStorage.lon = c.longitude;
                $scope.$apply();   
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma tu ubicación','Si sientes que el GPS no ha encontrado tu ubicación, puedes arrastrar el marcador hacia tu ubicación exacta');
                UI.getLocationName(c.latitude,c.longitude,function(dir){
                   $scope.selectFieldsFunction(dir);
                   $scope.gps = true;
                });                
             },function(e){
                 console.log("Error retrieving position " + e.code + " " + e.message);
                 alert('No se pudo comunicar con el gps por lo que se usará la ultima posición conocida');
                 $scope.gotoLocation(localStorage.lat, localStorage.lon);
             },$scope.options);
                         
             $scope.gotoLocation = function (lat, lon) {            
                if ($scope.lat !== lat || $scope.lon !== lon) {
                        $scope.basel = { lat: lat, lon: lon };
                        if (!$scope.$$phase)$scope.$apply("basel");
                    }
                };
            }
            else{
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Confirma tu ubicación','Pulsa sobre alguno de los iconos del mapa para saber cual es tu ubicación');
            }
    };
        
    $scope.location = function(value){
        $scope.basel = value;
        if(!$scope.$$phase){
            $scope.$apply("basel");
        }
        if($scope.gps){
            UI.getLocationName(value.lat,value.lon,function(dir){
                $scope.selectFieldsFunction(dir);
                $scope.confirmar();
             });
        }else{
            $scope.confirmar();
        }
    };
    
     // check login code
    $ionicPlatform.ready(function() {
            $scope.updatePosition();             
    });
    
    $scope.insertarPedido = function(){
        $scope.data = Api.datos;
        if($scope.data.fecha_solicitud===undefined){
            var fecha = new Date();
            var f = fecha.getFullYear()+'-'+(parseInt(fecha.getMonth())+1)+'-'+fecha.getDate()+' '+fecha.getHours()+':'+fecha.getMinutes()+':'+fecha.getSeconds();
            $scope.data.fecha_solicitud = f;
        }
        $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando solicitud...');
        if(!$scope.gps){
            $scope.unirDireccion();
        }
        
        Api.insert('pedidos',$scope,$http,function(data){
            Api.datos = null;
            Api.lista = undefined;
            if(Api.programar){
                document.location.href="#/tab/main";
            }else{
                document.location.href="#/tab/AsignarRepartidor/"+data.insert_primary_key;
            }
        });
    };
    
    $scope.confirmar = function(data){
        Api.datos.lat = $scope.basel.lat;
        Api.datos.lon = $scope.basel.lon;
        Api.datos.clientes_id = $scope.user;
        Api.datos.ubicacion = '('+$scope.basel.lat+','+$scope.basel.lon+')';
        if(!$scope.gps){
            Api.datos.direccion+= ' Calle: '+Api.datos.calle+' Ext: '+Api.datos.ext+' Colonia: '+Api.datos.colonia+' Nro. Int: '+Api.datos.int+' Delegación: '+Api.datos.delegacion+' Ciudad: '+Api.datos.ciudad+' CP:'+Api.datos.cp;
        }
        Api.datos.fecha_solicitud = $scope.fecha_solicitud;        
        Api.datos.status = 1;
        console.log(Api.datos.direccion);
        //Solicitar datos de entrega
        $scope.insertarPedido();
    };
    
    $scope.cancelar = function(){
        $state.go('tab.envio');
    };
    
    $scope.lugarEnfocado = 0;
    $scope.lugares = [];
    $scope.navegarlugares = function(lugares){        
        $scope.lugarEnfocado = 0;
        $scope.lugares = lugares;
        $scope.gotoLocation(lugares[0].geometry.location.lat(),lugares[0].geometry.location.lng());
        if(lugares.length>1){
            $scope.navigate = true;
            $scope.widthButtonConfirmar = '28%';
            $scope.widthButtonCancelar = '26%';
        }
    };
    $scope.seleccionarIzquierda = function(){
        $scope.lugarEnfocado -= 1;
        if($scope.lugarEnfocado===-1){
            $scope.lugarEnfocado = $scope.lugares.length-1;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
    $scope.seleccionarDerecha = function(){
        $scope.lugarEnfocado += 1;
        if($scope.lugarEnfocado>=$scope.lugares.length){
            $scope.lugarEnfocado = 0;
        }
        var p = $scope.lugares[$scope.lugarEnfocado].geometry.location;
        $scope.gotoLocation(p.lat(), p.lng());
    };
})

.controller('buscarRepartidor', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {    
    $scope.datos = Api.datos;        
    $scope = User.getData($scope);               
    $scope.loading = UI.getLoadingBox($ionicLoading);
})

.controller('Cuenta', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading);
    var f = $scope.fecha_vencimiento;
    f = f.split('-');    
    $scope.tarjetin = '***********'+$scope.tarjeta.substring(12);
    $scope.datos = {
        password:$scope.password,
        contrato:$scope.contrato,
        telefono_fijo:$scope.telefono,
        telefono_celular:$scope.celular,
        nombre_cliente:$scope.nombre,
        responsable:$scope.responsable,        
        nombre_tarjeta:$scope.nombre_tarjeta,
        cvc:$scope.cvc,
        mes:parseInt(f[0]),
        anio:parseInt(f[1])
    };
    
    $scope.field_types = {
        password:'password'
    };
    
    $scope.send = function(datos){
        if(datos.tarjeta===''){
            datos.tarjeta = $scope.tarjeta;
        }
        $scope.data = datos;        
        Api.update('clientes',$scope.user,$scope,$http,function(data){
            if(data.success){    
                $scope.showAlert = UI.getShowAlert($ionicPopup);
                $scope.showAlert('Sus datos','Sus datos han sido actualizados con éxito');
                User.setData($scope.data);
            }
        });
    };
})

.controller('EnviosList', function($scope,$state,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    
    $scope.filtro = 'Todos';
    $scope.refresh = function(){
            Api.list('pedidos',{clientes_id:$scope.user},$scope,$http,function(data){
                for(var i in data){
                    data[i].statusText = data[i].status==='1'?'Recogiendo':data[i].status==='2'?'En transito':'Completado';
                }
                $scope.lista = data;
                Api.lista = data;
                $scope.$broadcast('scroll.refreshComplete');
                $scope.filter($scope.filtro);
            });
    };
    $scope.refresh();
    
    $scope.filter = function(status){
        $scope.filtro = status;
        $scope.lista = [];
        for(var i=0; i<Api.lista.length; i++){
            if(status==='Todos' || status===Api.lista[i].status){
                $scope.lista.push(Api.lista[i]);
            }
        }
    };
})

.controller('EnviosDetail', function($scope,$state,$stateParams,$http,$ionicPlatform,$ionicSideMenuDelegate,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    
    $scope.asignar = function(){
        document.location.href = '#/tab/AsignarRepartidor/'+$scope.detail.id;
    };    
    $scope.anular = function(){
        $scope.confirm = UI.getConfirmBox($ionicPopup);
        $scope.loading = UI.getLoadingBox($ionicLoading,'Anulando Pedido');
        $scope.confirm('¿Estas seguro?','¿Estás seguro que deseas anular este pedido?, esta acción no podrá reversarse',function(){
           Api.deleterow('pedidos',$scope.detail.id,$scope,$http,function(){
               alert('Viaje anulado con éxito');
               $scope.$emit('travelcancel',{taxistas_id:$scope.detail.taxistas_id});
               document.location.href="#tab/envios";
           }); 
        })        
    };
    
    $scope.consultar = function(){
        if(Api.lista==undefined){
            $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
            Api.list('pedidos',{clientes_id:$scope.user,status:0},$scope,$http,function(data){
                Api.lista = data;
                for(i in Api.lista){
                    if(Api.lista[i].id==$stateParams.id){
                        $scope.detail = Api.lista[i];
                        $scope.basel = {lat: $scope.detail.lat, lon:$scope.detail.lon};
                        if(Api.buscarRepartidor){
                            $scope.asignar();
                        }
                    }
                }
                $scope.basel = {lat: $scope.detail.lat, lon:$scope.detail.lon};
            });
        }
        else{
            for(i in Api.lista){
                if(Api.lista[i].id==$stateParams.id){
                    $scope.detail = Api.lista[i];
                    $scope.basel = {lat: $scope.detail.lat, lon:$scope.detail.lon};
                }
            }
        }   
    };
    
    $scope.calificar = function(){
        Api.calificar = $scope.detail;  
        $state.go('tab.calificar');
    };
    
    $scope.consultar();
})

.controller('AsignarRepartidor', function($scope,$state,$stateParams,$http,$ionicLoading,$ionicPopup,Api,UI,User,SocketConnection) {
        /**
        * Created by PavelCSS on 13.01.15.
        */
       var time = 2;
       var peopleCount = 20;
       var peoples = [];

       for (i = 0; i < peopleCount; i++) {
           peoples.push({
               distance : Math.floor((Math.random() * 140) + 1),
               angle    : Math.floor((Math.random() * 360) + 1)
           });
       }

       (function radar(){

           var radius = 150;
           for (i = 0; i < peoples.length; i++) {
               var disX = 90 < peoples[i].angle + 90 < 270 ? radius - peoples[i].distance : radius,
                   disY = 180 < peoples[i].angle + 90 < 360 ? radius - peoples[i].distance : radius,
                   angleNew = (peoples[i].angle + 90) * Math.PI / 180,
                   getDegX = disX + peoples[i].distance - Math.round(peoples[i].distance * Math.cos(angleNew)),
                   getDegY = disY + peoples[i].distance - Math.round(peoples[i].distance * Math.sin(angleNew)),
                   delay = time / radius * (peoples[i].distance + 5);

               $('#guides').append($('<span>')
                   .addClass('dot')
                   .css({
                       left : getDegX,
                       top  : getDegY,
                       '-webkit-animation-delay' : delay + 's',
                       'animation-delay' : delay + 's'
                   })
                   .attr({
                       'data-atDeg' : peoples[i].angle
                   }));
             $("#sonar").addClass('animated');
           }
        })();
        
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);    
    $scope.reintentar = false;
    
    Api.respuestaSolicitudRepartidor = function(data){
        $scope.loading('hide');
        console.log(data);
        if(data.response){            
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            var str = '<div> Se ha encontrado un taxista</div>';
            str+= '<div class="card">';
            str+= '<img src="'+data.data.foto+'" style="width:100%">';
            str+= '<div><b>Nombre: </b>'+data.data.nombre+'</div>';
            str+= '<div><b>Placa: </b>'+data.data.placa+'</div>';
            $1 = parseInt(data.data.rank)>=1?'act':' ';
            $2 = parseInt(data.data.rank)>=2?'act':' ';
            $3 = parseInt(data.data.rank)>=3?'act':' ';
            $4 = parseInt(data.data.rank)>=4?'act':' ';
            $5 = parseInt(data.data.rank)>=5?'act':' ';
            str+= '<div align="center">\n\
            <ul class="rate2">\n\
                <li class="'+$1+'"></li>\n\
                <li class="'+$2+'"></li>\n\
                <li class="'+$3+'"></li>\n\
                <li class="'+$4+'"></li>\n\
                <li class="'+$5+'"></li>\n\
            </ul>\n\
            </div>';
            str+= '</div>';
            $scope.showAlert('Respuesta de asignación',str);               
            Api.lista=undefined;
            //document.location.href="#/tab/pedido/"+$stateParams.id;
            //document.location.href="#/tab/main";
            $state.go('tab.ruta');
        }
        else{
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            $scope.showAlert('Respuesta de asignación','Lo sentimos pero no hemos encontrado taxistas en este momento, su envió pasará a una lista de espera.');
            $scope.reintentar = true;
        }
    };
    $scope.asignar = function(){        
        $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Repartidor');
        $scope.loading('show');
        if(Api.programar){
            SocketConnection.programar($scope.detail.id,Api.time,function(){
                $scope.loading('hide');
                alert('Se ha programado su envio');
                document.location.href="#/tab/main";
            });
        }else{
            SocketConnection.buscarRepartidor($scope.detail.id,function(){
                $scope.loading('hide');
            });
        }
    };
    
    $scope.consultar = function(){
        $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
        Api.list('pedidos',{clientes_id:$scope.user,status:0},$scope,$http,function(data){
            Api.lista = data;
            for(var i in Api.lista){
                if(Api.lista[i].id===$stateParams.id){
                    $scope.detail = Api.lista[i];
                    $scope.asignar();
                }
            }                
        });        
    };
    
    $scope.consultar();
})

.controller('Mensajes', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    Api.list('pedidos',{clientes_id:$scope.user,'pedidos.taxistas_id != ':'null'},$scope,$http,function(data){
        Api.lista = data;
        $scope.lista = data;
    },'where');
})

.controller('ReadMensajes', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
    $scope.consultar = function(){
        if(Api.lista===undefined){
            Api.list('pedidos',{clientes_id:$scope.user},$scope,$http,function(data){
                Api.lista = data;
                $scope.lista = data;
            },'where');
        }
        if(Api.mensaje===undefined){
            $scope.data = {'pedido_id':$stateParams.id};
            Api.query('getMensajes',$scope,$http,function(data){
                
                if(typeof(data)!=='object'){
                    data = JSON.parse(JSON.parse(data));
                    $scope.mensajes = data;
                    if(!$scope.$$phase){
                        $scope.$apply();
                    }
                }
                else{
                    $scope.mensajes = [];
                }
            });
        }
         $scope.$broadcast('scroll.refreshComplete');
    };
    
    $scope.mensaje = '';
    
    $scope.sendMensaje = function(mensaje){
        $scope.mensaje = '';
        var dest = 0;
        for(var k in Api.lista){            
            if(Api.lista[k].id===$stateParams.id){
                dest = Api.lista[k].taxistas_id;
            }
        }
        var data = {userId:$scope.user,userName:$scope.nombre,mensaje:mensaje,dest:dest,pedido_id:$stateParams.id};        
        $scope.mensajes.push(data);
        $scope.$emit('sendmensajetoserver',data);
    };
    
    $scope.$on('message_receiver',function(evt,data){
        $scope.mensajes.push(data);
        if(!$scope.$$phase){
            $scope.$apply();
        }
    });
    
    $scope.consultar();
})

.controller('Calificar', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);        
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando Datos');
    Api.list('taxistas',{id:$stateParams.id},$scope,$http,function(data){
        $scope.detail = data[0];
        $scope.foto = 'http://taxi.ocupotaxi.mx/images/taxistas/'+$scope.detail.foto;
        if(!$scope.$$phase){
            $scope.$apply();
        }
    },'where');    
    $scope.datos = {calidad:0};
    $scope.detail = {nombre_taxista:'Buscando'}
    $scope.rankear = function(datos){
        $scope.data = {calificacion:datos.calidad,taxistas_id:$stateParams.id};
        Api.insert('calificaciones',$scope,$http,function(data){
           $scope.showAlert = UI.getShowAlert($ionicPopup);
           $scope.showAlert('Se ha enviado su calificación con éxito');
           $state.go('tab.main');
        });
    };
})

.controller('Ruta', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.basel = {lat:localStorage.lat,lon:localStorage.lon};
    $scope.iconMap = 'http://taxi.ocupotaxi.mx/img/person.png';
    $scope.$on('refreshTaxistaCtrl',function(evt,data){
        console.log(data);
        $scope.whoiswhere = [];
        $scope.whoiswhere.push({ "id":0,"name":'Taxis asignado', "lat": data.lat, lon: data.lon,datos:{},icon:'http://taxi.ocupotaxi.mx/img/taxiAlerta.png'});
        $scope.$apply('basel');
    });
});