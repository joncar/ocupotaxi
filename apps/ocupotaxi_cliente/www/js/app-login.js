// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var MapApp = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngOpenFB'])

.run(function($ionicPlatform,ngFB) {
  ngFB.init({appId: '801758506602186'});
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs-login.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.login', {
    url: '/login',
    views: {
      'tab-login': {
        templateUrl: 'templates/login.html',
        controller: 'Login'
      }
    }
  })
  
  .state('tab.registrar', {
    url: '/registrar',
    views: {
      'tab-login': {
        templateUrl: 'templates/registrar.html',
        controller: 'Registrar'
      }
    }
  })
  
  .state('tab.registrar2', {
    url: '/registrar2',
    views: {
      'tab-login': {
        templateUrl: 'templates/registrar2.html',
        controller: 'Registrar'
      }
    }
  })
  
  .state('tab.registrar3', {
    url: '/registrar3',
    views: {
      'tab-login': {
        templateUrl: 'templates/registrar3.html',
        controller: 'Registrar'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/login');

});
