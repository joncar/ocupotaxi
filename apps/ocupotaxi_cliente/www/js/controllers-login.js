angular.module('starter.controllers', ['ngOpenFB'])

.controller('Login', function($scope,$http,$ionicLoading,$ionicPopup,User,Api,UI) {
       $scope.loading = UI.getLoadingBox($ionicLoading);       
       $scope.showAlert = UI.getShowAlert($ionicPopup);       
       $scope.validEnter = function(){
            if(localStorage.email!==undefined){                     
               $scope = User.getData($scope);
               document.location.href="main.html";
            }
       }

       $scope.login = function(param){
            $scope.email = param.email;
            $scope.password = param.password;
            $scope.isRegister();
       }

       $scope.isRegister = function(){
           data =  {email:$scope.email,password:$scope.password};           
           Api.list('clientes',data,$scope,$http,function(data){
                 if(data.length==0){                   
                    $scope.showAlert('Inicio de sesión','Email o Contraseña Incorrecta');
                 }
                 else{
                       data = data[0];
                       User.setData(data);
                       document.location.href="main.html";
                 }
           });         
       }
       $scope.validEnter();
})

.controller('Registrar', function($scope,$state,$http,$ionicLoading,$ionicPopup,User,Api,UI,ngFB) {
            $scope.loading = UI.getLoadingBox($ionicLoading);       
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            $scope.data = {};
            $scope.fbLogin = function () {
            ngFB.login({scope: 'email,user_location'}).then(
                function (response) {
                    if (response.status === 'connected') {                        
                        $scope.getInfo();
                    } else {
                        alert('Facebook login failed');
                    }
                });
            };
            
            $scope.getInfo = function() {
                ngFB.api({path: '/me',params: {fields: 'id,name,location,email'}}).then(
                    function(user) {
                        console.log(JSON.stringify(user));
                        $scope.data = {email:user.email,responsable:user.name,calle:user.location.name};
                        if(!$scope.$$phase()){
                            $scope.$apply();
                        }
                    },
                    function(e){
                        console.log(e);
                    });
            }
        
            $scope.personales = function(data){
              if(data!=undefined){
                 
                 if(data.ciudad!=undefined){
                    data.direccion = data.ciudad+' '+data.calle;
                    Api.data = data;
                    Api.data.condiciones = null;
                    Api.insert('clientes',$scope,$http,function(data){
                        if(data.success){
                            $scope.showAlert('Sus datos han sido guardados');
                            $scope.data.id = data.insert_primary_key;
                            User.setData($scope.data);
                            document.location.href="main.html";
                        }
                      });
                 }
                 else{
                     $scope.showAlert('Debe indicar sus datos personales para continuar');
                 }
              }  
              else{
                    $scope.showAlert('Debe indicar sus datos personales para continuar');
              }
            };        
});
