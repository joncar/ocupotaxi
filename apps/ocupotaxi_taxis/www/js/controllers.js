angular.module('starter.controllers', [])

.controller('Main', function($scope,$state,$http,$ionicPlatform,$ionicPopup,Api,UI,User) {
    $scope.options = { enableHighAccuracy: true,  };
    navigator.geolocation.watchPosition(function(position) {
        $scope.position=position;
        var c = position.coords;
        Api.position = {lat:c.latitude, lon:c.longitude};
        localStorage.lat = c.latitude;
        localStorage.lon = c.longitude;
        $scope.$broadcast('gotoLocation',c.latitude,c.longitude);
        //$scope.$apply();
    },function(e) { console.log("Error retrieving position " + e.code + " " + e.message);},$scope.options);
             
    $scope = User.getData($scope);
    $scope.iconMap = 'http://taxi.ocupotaxi.mx/img/taxiAlerta.png';
    $scope.getMarks = function(){
        Api.list('pedidosMotos',{taxistas_id:$scope.user},$scope,$http,function(data){
            $scope.whoiswhere = [];
            for(var i in data){
                var lat = data[i].status==='1'?data[i].lat:data[i].lat_entrega;
                var lon = data[i].status==='1'?data[i].lon:data[i].lon_entrega;
                
                var icon = 'http://taxi.ocupotaxi.mx/img/person.png';
                
                $scope.whoiswhere.push({ "id":data[i].id,"name":'Nro Pedido: '+data[i].id, "lat": lat, lon: lon,datos:data[i],icon:icon});
                if (!$scope.$$phase){
                    $scope.$apply('whoiswhere');
                    $scope.$apply();
                }
            }
            
            if($scope.whoiswhere.length>0){
                Api.clientes_id = data[0].clientes_id;
                $scope.$broadcast('gotoLocation',$scope.whoiswhere[0].lat,$scope.whoiswhere[0].lon);
            }
            $scope.$broadcast('detailMainPedidos',data[0]);
        });
    };
    
    $scope.moveMarkerManually = function(val){
        $scope.$emit('gotoLocation',val);
    };
    
    $scope.$on('refresh',function(){
       $scope.getMarks();
    });
    $scope.$on('mainRefreshMap',function(){
        document.location.reload();
    });
    $scope.pedidoclick = function(mark){
        document.location.href="#/tab/pedidos/"+mark.id;
    };  
    
    $ionicPlatform.ready(function(){
        $scope.$broadcast('refresh');
        if(typeof(cordova)!=='undefined'){
                //Background
                // Android customization
                cordova.plugins.backgroundMode.setDefaults({ title:'DeliveryMix', text:'Taxista Activo'});
                // Enable background mode
                cordova.plugins.backgroundMode.enable();
                cordova.plugins.backgroundMode.onactivate = function () {
                    cordova.plugins.backgroundMode.configure({
                        text:'Taxista Activo'
                    });
                };
        }
    });
})

.controller('Menu', function($scope,$state,$http,$ionicPlatform,$ionicPopup,Api,UI,User,SocketConnection,$ionicSideMenuDelegate) { 
    if(localStorage.user===undefined){
        document.location.href="index.html";
    }
    
    $scope = User.getData($scope);
    
    $scope.whoiswhere = [];        
    $scope.basel = { lat: 16.7417092, lon: -93.0926997 };
    Api.position = { lat: 16.7417092, lon: -93.0926997 };
    if(localStorage.lat!==undefined){
        $scope.basel.lat = localStorage.lat;
        Api.position.lat = localStorage.lat;
        $scope.basel.lon = localStorage.lon;
        Api.position.lon = localStorage.lon;
    }
    $scope = User.getData($scope);        
    $scope.workin =Api.workin===undefined?true:Api.workin;
    $scope.siniestro = false;
    $scope.carga = false;
    $scope.refreshDataToServer = {};
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.confirm = UI.getConfirmBox($ionicPopup);
    $scope.updatePosition = function(){
        if($scope.workin){
             if(Api.position!==undefined){
                $scope.gotoLocation(Api.position.lat,Api.position.lon);
                $scope.refreshLocation();                
             }
             $scope.checkNewPaquetes();
        }
        clearTimeout($scope.timer);
        $scope.timer = setTimeout($scope.updatePosition,10000);             
    };
    
    $scope.gotoLocation = function (lat, lon) {        
        if ($scope.basel.lat !== lat || $scope.basel.lon !== lon) {
            $scope.basel = { lat: lat, lon: lon };
            if (!$scope.$$phase){
                $scope.$apply("basel");
                $scope.$apply();
            }
        }
    };
    
    $scope.$on('gotoLocation',function(evt,val){
        console.log(val);
        Api.position = val;
        $scope.gotoLocation(val.lat,val.lon);
    });
    
    $scope.checkNewPaquetes = function(){
        console.log('Carga chequeada');
    };
    
    $scope.refreshLocation = function(){        
        $scope.refreshDataToServer.lat = Api.position.lat;
        $scope.refreshDataToServer.lon = Api.position.lon;     
        $scope.updateServer();
    };
    
    $scope.updateServer = function(){
        $scope.refreshDataToServer.status = $scope.getStatus();
        $scope.refreshDataToServer.id = $scope.user;
        $scope.data = $scope.refreshDataToServer;
        Api.query('refreshLocation',$scope,$http,function(data){
            console.log(data);
        });
        if(typeof(ws)!=='undefined'){
            ws.emit('latLng',{lat:Api.position.lat,lon:Api.position.lon,clientes_id:Api.clientes_id});
        }
    };
    
    $scope.getStatus = function(){
        re = 5;
        if($scope.workin){
            re = 1;
        }        
        if($scope.siniestro){
            re = 4;
        }            
        return re;
    };
    
    //Botones
    $scope.toggleWorkin = function(){        
        $scope.workin = $scope.workin?false:true;
        Api.workin = $scope.workin;
        if($scope.siniestro){
            $scope.workin = false;
        }
        if($scope.workin){
            $scope.updatePosition();
        }
        else{
            $scope.updateServer();
            clearTimeout($scope.timer);
        }
        
        if(!$scope.$$phase){
            $scope.$apply('workin');
            $scope.$apply();
        }
    };
    
    $scope.toggleSiniestro = function(){
        $scope.siniestro = $scope.siniestro?false:true;
        $scope.updateServer();
        if($scope.siniestro && $scope.workin){
            $scope.toggleWorkin();
        }
        if(!$scope.$$phase){
            $scope.$apply('siniestro');
            $scope.$apply();
        }
    };
    
    $scope.toggleCarga = function(){
        $scope.carga = $scope.carga?false:true;        
        if(!$scope.$$phase){
            $scope.$apply('carga');
            $scope.$apply();
        }
    };
    
    $scope.connecting = false;
    $scope.connected = function(){$scope.connecting = true; if(!$scope.$$phase){$scope.$apply();}};
    $scope.disconnected = function(){$scope.connecting = false; if(!$scope.$$phase){$scope.$apply();}};
    SocketConnection.WebSocketOpen($scope);
    SocketConnection.connectID($scope.user);
    
    ws.on('onElegido',function(data){
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    text:'Nueva solicitud de viaje.'
                });
                Api.notificationSound.play();
        }
        if(Api.progress===undefined){
            Api.progress = true;
            $scope.idPedido = data.idPedido;
            Api.list('pedidos',{id:data.idPedido},$scope,$http,function(data){
                $scope.confirm('Nueva solicitud de viaje','Haz recibido una solicitud de viaje ¿Deseas aceptarla?',function(){
                    ws.emit('responderSolicitud',{response:true,idPedido:$scope.idPedido,foto:$scope.foto,placa:$scope.placa,nombre:$scope.nombre,rank:$scope.rank});
                    $scope.refreshMarks();
                    Api.progress = undefined;
                    $scope.$broadcast('refresh');
                },function(){
                    ws.emit('responderSolicitud',{response:false,idPedido:$scope.idPedido});
                    Api.progress = undefined;
                });
            });
        }
    });
    
    ws.on('message_receiver',function(data){
        if(typeof(cordova)!=='undefined' && cordova.plugins.backgroundMode.isActive()){
                cordova.plugins.backgroundMode.configure({
                    text:'Mensaje recibido.'
                });
                Api.notificationSound.play();
        }
        
       if($state.current.name==='tab.mensajesRead'){
           $scope.$broadcast('message_receiver',data);
       }
       else{
           var myPopup = $ionicPopup.show({
                template: data.mensaje,
                title: 'Haz recibido un mensaje',
                subTitle: 'Nro. Pedido: '+data.pedido_id,
                scope: $scope,
                buttons: [
                    {
                      text: '<b>Cerrar</b>',              
                      onTap: function(e) {                        
                          myPopup.close();
                      }
                    },
                    {
                      text: '<b>Ver Mensaje</b>',
                      onTap: function(e) {                        
                        document.location.href="#/tab/mensajes/"+data.pedido_id;                        
                      }
                    }
                  ]
          });
       }
    });
    
    ws.on('travelcancel',function(){
        alert('El cliente ha cancelado la solicitud');
        document.location.reload();
    });
    
    $scope.refreshMarks = function(){
        $scope.$emit('refresh','');
    };
    
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };
    
    $scope.$on('sendmensajetoserver',function(evt,data){
        ws.emit('sendMessage',data);
    });
    
    $scope.$on('finishEntrega',function(evt,data){
        ws.emit('finishEntrega',data);
    });
    
    $scope.desconectar = function(){
        User.cleanData();
        document.location.href="index.html";
    };
    
    // check login code
    $ionicPlatform.ready(function() {
        $scope.updatePosition();  
        $scope.gotoLocation(Api.position.lat,Api.position.lon);
        $scope.checkNewPaquetes();
        if(typeof(Media)!=='undefined'){
            Api.notificationSound = new Media('/android_asset/www/sounds/notificacion.mp3',function(){},function(e){alert("Error getting pos="+e);});
            Api.notificationSound.play();
        }
    });
})


.controller('Pedidos', function($scope,$state,$http,$stateParams,$ionicPlatform,$ionicPopup,$ionicLoading,Api,UI,User) {    
    
    if(typeof($stateParams.id)!=='undefined'){
        Api.list('pedidos',{taxistas_id:$scope.user,id:$stateParams.id},$scope,$http,function(data){
            $scope.detail = data[0];
        });
    }    
    $scope.$on('detailMainPedidos',function(evt,data){
       $scope.detail = data;
    });
    
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Actualizando Pedido');
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.sendit = false;
    
    $scope.notificarRecogida = function(){
        $scope.data = {status:2};
        $scope.detail.status = 2;
        Api.update('pedidos',$scope.detail.id,$scope,$http,function(data){           
           $state.go('tab.main');
        });
    };
    
    $scope.notificarEntrega = function(){
        $scope.data = {status:3};
        $scope.detail.status = 3;
        Api.update('pedidos',$scope.detail.id,$scope,$http,function(data){
           $scope.$emit('finishEntrega',{sucursales_id:$scope.detail.sucursales_id,pedidos_id:$stateParams.id,taxistas_id:$scope.user});
           $state.go('tab.main');
           $scope.$emit('mainRefreshMap',{});
        });
    };
})

.controller('Envios', function($scope,$state,$http,$ionicPlatform,$ionicPopup,$ionicLoading,Api,UI,User,SocketConnection){
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Actualizando Pedido');
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    
    $scope.refresh = function(){
        Api.list('pedidos',{taxistas_id:$scope.user},$scope,$http,function(data){
            for(var i in data){
                data[i].statusText = data[i].status==='1'?'Recogiendo':data[i].status==='2'?'En transito':'Completado';
            }
            $scope.lista = data;
            Api.lista = data;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.filter($scope.filtro);
        });
    };    
    $scope.refresh();
    $scope.seleccionarPedido = function(id){
        Api.selected = {datos:id};
        document.location.href="#/tab/pedidos/"+id.id;
    };
    
    $scope.filter = function(status){
        $scope.lista = [];
        for(var i in Api.lista){
            if(status==='Todos' || status===Api.lista[i].status){
                $scope.lista.push(Api.lista[i])
            }
        }
    };
})

.controller('Mensajes', function($scope,$state,$http,$ionicPlatform,$ionicLoading,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.consult = function(){
        Api.list('pedidos',{taxistas_id:$scope.user},$scope,$http,function(data){
            Api.lista = data;
            $scope.lista = data;
        },'where');
    }
    $scope.consult();
})

.controller('ReadMensajes', function($scope,$state,$http,$ionicLoading,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);    
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    
    $scope.consultar = function(){
        if(Api.lista===undefined){
            Api.list('pedidos',{taxistas_id:$scope.user},$scope,$http,function(data){
                Api.lista = data;
                $scope.lista = data;
            },'where');
        }
        if(Api.mensaje===undefined){
            $scope.data = {'pedido_id':$stateParams.id};
            Api.query('getMensajes',$scope,$http,function(data){    
                if(typeof(data)!=='object'){
                    data = JSON.parse(JSON.parse(data));
                    $scope.mensajes = data;
                    if(!$scope.$$phase){
                        $scope.$apply();
                    }
                }
                else{
                    $scope.mensajes = [];
                }
            });
        }
       $scope.$broadcast('scroll.refreshComplete');
    };
    
    $scope.mensaje = '';
    
    $scope.sendMensaje = function(mensaje){
        $scope.mensaje = '';
        var dest = 0;        
        for(var i in Api.lista){            
            if(Api.lista[i].id===$stateParams.id){
                dest = Api.lista[i].clientes_id;
            }
        }
        var data = {userId:$scope.user,userName:$scope.nombre,mensaje:mensaje,dest:dest,pedido_id:$stateParams.id};          
        $scope.mensajes.push(data);
        $scope.$emit('sendmensajetoserver',data);
    };
    
    $scope.$on('message_receiver',function(evt,data){
        $scope.mensajes.push(data);
        if(!$scope.$$phase){
            $scope.$apply();
        }
    });
    
    $scope.consultar();
});
